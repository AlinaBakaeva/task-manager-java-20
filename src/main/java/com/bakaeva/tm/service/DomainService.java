package com.bakaeva.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.api.service.IProjectService;
import com.bakaeva.tm.api.service.ITaskService;
import com.bakaeva.tm.api.service.IUserService;
import com.bakaeva.tm.dto.Domain;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
    }

}