package com.bakaeva.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;

public final class DataJsonLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_JSON);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}