package com.bakaeva.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataXmlClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove xml data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}