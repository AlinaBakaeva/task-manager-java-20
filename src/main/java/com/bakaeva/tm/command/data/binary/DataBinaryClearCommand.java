package com.bakaeva.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}