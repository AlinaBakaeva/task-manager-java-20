package com.bakaeva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Project;
import com.bakaeva.tm.util.TerminalUtil;

public final class ProjectByIdRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().removeById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}