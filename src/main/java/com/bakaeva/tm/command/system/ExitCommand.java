package com.bakaeva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }


    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}