package com.bakaeva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.util.TerminalUtil;

public final class PasswordUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "password-update";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update my password.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().updatePasswordById(userId, password);
        System.out.println("[OK]");
    }

}