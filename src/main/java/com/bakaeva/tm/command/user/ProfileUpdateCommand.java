package com.bakaeva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.util.TerminalUtil;

public final class ProfileUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "profile-update";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update my profile.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserService().updateById(
                userId, login,
                firstName, lastName, middleName,
                email
        );
        System.out.println("[OK]");
    }

}