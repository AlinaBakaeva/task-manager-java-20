package com.bakaeva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void export(@Nullable Domain domain);

}
