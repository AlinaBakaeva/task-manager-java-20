package com.bakaeva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        entities.remove(project);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> userProjects = findAll(userId);
        entities.removeAll(userProjects);
    }

        @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@Nullable final Project project : entities) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) result.add((project));
        }
        return result;
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final Project project : entities) {
            if (project == null) continue;
            if (id.equals((project.getId())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final Project project : entities) {
            if (project == null) continue;
            if (name.equals((project.getName())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    @Nullable
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}