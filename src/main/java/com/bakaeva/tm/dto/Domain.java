package com.bakaeva.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import com.bakaeva.tm.entity.Project;
import com.bakaeva.tm.entity.Task;
import com.bakaeva.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class Domain implements Serializable {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

}