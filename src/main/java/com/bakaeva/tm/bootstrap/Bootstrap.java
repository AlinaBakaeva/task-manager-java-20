package com.bakaeva.tm.bootstrap;

import com.bakaeva.tm.constant.ReflectionConstant;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.api.repository.IUserRepository;
import com.bakaeva.tm.api.service.*;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.system.UnknownCommandException;
import com.bakaeva.tm.repository.ProjectRepository;
import com.bakaeva.tm.repository.TaskRepository;
import com.bakaeva.tm.repository.UserRepository;
import com.bakaeva.tm.service.*;
import com.bakaeva.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator{

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService,
            projectService,
            taskService
    );

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void initCommands() throws IllegalAccessException, InstantiationException {
        @NotNull final Reflections reflections = new Reflections(ReflectionConstant.COMMAND_PACKAGE_PATH);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());

        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initData() {
        @NotNull final User user = userService.create("test", "test", "test@test.ru");
        taskService.create(user.getId(), "UserTask1");
        taskService.create(user.getId(), "UserTask2");
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(admin.getId(), "AdminTask1");
        taskService.create(admin.getId(), "AdminTask2");
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("*** Welcome to task manager ***");
        try {
            if (parseArguments(arguments)) System.exit(0);
            initCommands();
            initData();
        } catch (Exception e) {
            logError(e);
        }
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull final Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void parseArgument(@Nullable final String argument) throws Exception {
        if (argument == null || argument.isEmpty()) return;
        @Nullable final AbstractCommand command = getCommandByArgument(argument);
        if (command == null) return;
        command.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        if (roles != null) {
            authService.checkRoles(command.roles());
        }
        command.execute();
    }

    public boolean parseArguments(@Nullable final String[] arguments) throws Exception {
        if (arguments == null || arguments.length == 0) return false;
        @Nullable final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Nullable
    private AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (@NotNull final AbstractCommand command : commands.values()) {
            if (argument.equals(command.argument())) return command;
        }
        return null;
    }

}